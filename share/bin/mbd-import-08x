#!/usr/bin/python3
# pylint: disable=invalid-name  # https://github.com/PyCQA/pylint/issues/516

import os
import re
import tempfile
import subprocess
import argparse

import debian.deb822
import debian.debian_support


def call(cmd, show=True, dry_run=False):
    if show:
        print(f"Calling (dry_run={dry_run}): {cmd}")

    if dry_run:
        return 0, f"Dry run, skipped: {cmd}"

    output = tempfile.TemporaryFile()  # pylint: disable=redefined-outer-name
    res = subprocess.call(cmd.split(), stdout=output, stderr=subprocess.STDOUT)  # pylint: disable=redefined-outer-name
    output.seek(0)
    output_str = output.read().decode("UTF-8")
    if show:
        print(f"Result={res}:")
        print(output_str)
    return res, output_str


def cmp_versions(pair0, pair1):
    """Compare Debian package versions (on first item of pair)."""
    return debian.debian_support.version_compare(pair0[0], pair1[0])


PARSER = argparse.ArgumentParser(prog="mbd-import-08x",
                                 description="Import package from an old mini-buildd 08x distribution into a reprepro distribution in cwd.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

PARSER.add_argument("path", help="complete path to a 08x dist repo")
PARSER.add_argument("distribution", help="reprepro distribution to import to")
PARSER.add_argument("-n", "--dry-run", action='store_true',
                    help="dry run, just show what commands would be run")

# Parse args
ARGS = PARSER.parse_args()

# Check we are in a reprepro dist
assert os.path.exists("conf/distributions"), "No reprepro repository detected in cwd."

# Check 08x path
OLD_DIST = os.path.basename(ARGS.path)
OLD_SPLIT = OLD_DIST.split("-")
assert len(OLD_SPLIT) >= 2 and len(OLD_SPLIT) <= 3, f"Malformed 08x dist '{OLD_DIST}'"

OLD_CODENAME, OLD_REPOID = OLD_SPLIT[0], OLD_SPLIT[1]

CODENAME, REPOID, _SUITE = ARGS.distribution.split("-")
assert OLD_CODENAME == CODENAME, f"08x and 1.0 base dist (codename) are DIFFERENT: {OLD_CODENAME} vs {CODENAME}"

print(f"""
Parsing packages from '{ARGS.path}'...
""")

# { PACKAGE: {VERSION: {"dsc": dsc, 'debs': [deb, deb,...]}}
PACKAGES = {}

for dsc in debian.deb822.Sources.iter_paragraphs(open(os.path.join(ARGS.path, "Sources"))):
    print("Scanning source", dsc["package"], dsc["version"])
    v = PACKAGES.setdefault(dsc["package"], {})
    v[dsc["version"]] = {"dsc": dsc, "debs": []}
    for deb in debian.deb822.Packages.iter_paragraphs(open(os.path.join(ARGS.path, "Packages"))):
        if deb.get("source", deb["package"]) == dsc["package"] and deb["version"] == dsc["version"]:
            v[dsc["version"]]["debs"].append(deb)

if OLD_REPOID != REPOID:
    input(f"""
WARN: Old/new repo ids different: '{OLD_REPOID}' vs '{REPOID}'.
WARN: If this is just a typo, STOP HERE.
WARN: If you did not create a new repo with the same id, and/or want to force the packages in, force continue.
WARN: Continue (Ctr-C to cancel)?""")

print(f"""
{OLD_DIST}: {len(PACKAGES)} source packages. Trying to import to '{ARGS.distribution}' in reprepo/cwd
""")
input(f"Start import (dry_run={ARGS.dry_run}) (Ctrl-C to cancel)?")
print()

for package, versions in PACKAGES.items():
    # The only possible check is to check that repropro's ls output is empty
    res, output = call(f"reprepro list {ARGS.distribution} {package}", show=False)
    if output:
        print(f"** Skipping {package}: Already in dist '{ARGS.distribution}'")
    else:
        print(f"** Importing {package}")
        r = -1
        for v, items in sorted(list(versions.items()), cmp=cmp_versions, reverse=True):
            dist = ARGS.distribution
            if r >= 0:
                dist += f"-rollback{r}"
            r += 1
            print(f"* Importing {package}-{v} to {dist}")

            dsc_file = None
            for s in items["dsc"]["files"]:
                if re.compile(r"^.*\.dsc$").match(s["name"]):
                    dsc_file = os.path.join(ARGS.path, os.path.basename(s["name"]))
                    break

            res, output = call(f"reprepro includedsc {dist} {dsc_file}", dry_run=ARGS.dry_run)
            if res != 0:
                print("WARN: includedsc failed; retrying with priority/section=extra/misc...")
                call(f"reprepro --priority extra --section misc includedsc {dist} {dsc_file}", dry_run=ARGS.dry_run)

            for deb in items["debs"]:
                deb_file = os.path.join(ARGS.path, os.path.basename(deb["filename"]))
                _dummy, ext = os.path.splitext(deb_file)
                typ = ext[1:]
                call(f"reprepro include{typ} {dist} {deb_file}", dry_run=ARGS.dry_run)
    print()
