import sys
import logging

DEBUG = []
FOREGROUND = False

HOSTNAME = None
HTTP_ENDPOINTS = []


def primary_http_endpoint():
    return HTTP_ENDPOINTS[0]


PATH_API = "mini_buildd/api"
PATH_LOGIN = "accounts/login/"
PATH_LOGIN_NEXT = "mini_buildd/"


#: Global directory paths
HOME_DIR = None

INCOMING_DIR = None
REPOSITORIES_DIR = None

SPOOL_DIR = None
TMP_DIR = None
LOG_DIR = None
LOG_FILE = None
ACCESS_LOG_FILE = None
CHROOTS_DIR = None
CHROOTS_LIBDIR = None

MANUAL_DIR = None

PACKAGE_TEMPLATES_DIR = "/usr/share/mini-buildd/package-templates"

#: This should never ever be changed
CHAR_ENCODING = "UTF-8"

# Compute python-version dependent install path
PY_PACKAGE_PATH = f"/usr/lib/python{sys.version_info[0]}/dist-packages"


def log_exception(log, message, exception, level=logging.ERROR):
    msg = f"{message}: {exception}"
    log.log(level, msg)
    if "exception" in DEBUG:
        log.exception(f"Exception DEBUG ({msg}):")
